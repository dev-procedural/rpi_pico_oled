from machine import Pin,PWM
import time
from driver import LCD_1inch14

import network
# import rp2
import machine
import socket

from umqtt.simple import MQTTClient

BL = 13

mqtt_server = "192.168.2.250"
# rp2.country('US')

def WIFI():
	wlan = network.WLAN(network.STA_IF)
	wlan.active(True)

	ssid = secrets['ssid']
	pw = secrets['pw']

	wlan.connect(ssid, pw)
	wlan.ifconfig(('192.168.2.222', '255.255.255.0', '192.168.2.1', '192.168.2.250'))

	timeout = 10
	while timeout > 0:
			if wlan.status() < 0 or wlan.status() >= 3:
					break
			timeout -= 1
			print('Waiting for connection...')
			time.sleep(1)

	wlan_status = wlan.status()
	if wlan_status != 3:
			raise RuntimeError('Wi-Fi connection failed')
	else:
			print('Connected')
			status = wlan.ifconfig()
			print('ip = ' + status[0])

def connectMQTT():
    server= mqtt_server
    client = MQTTClient(client_id=b"picow2",
        server=server,
        port=1883,
        keepalive=7200,
    )
    
    client.connect()
    return client

# WIFI() 
# mqtt = connectMQTT()


def menu(CURRENT,DIR):
    OPTS = ["POOP", "BATH", "FEED"]
    
    if DIR == "UP":
        CURRENT = OPTS[OPTS.index(CURRENT) - 1]
        LCD.text("Poop",120,40,LCD.green if CURRENT == "POOP" else LCD.red)
        LCD.text("Bath",120,60,LCD.green if CURRENT == "BATH" else LCD.red)
        LCD.text("Feed",120,80,LCD.green if CURRENT == "FEED" else LCD.red)
        
        time.sleep(0.3)
        print(CURRENT)
        return CURRENT

    elif DIR == "DOWN":
        CURRENT = OPTS[OPTS.index(CURRENT) + 1]
        LCD.text("Poop",120,40,LCD.green if CURRENT == "POOP" else LCD.red)
        LCD.text("Bath",120,60,LCD.green if CURRENT == "BATH" else LCD.red)
        LCD.text("Feed",120,80,LCD.green if CURRENT == "FEED" else LCD.red)
    
        time.sleep(0.3)
        return CURRENT


if __name__=='__main__':

    CURRENT = "POOP"

    pwm = PWM(Pin(BL))
    pwm.freq(1000)
    pwm.duty_u16(32768)#max 65535

    LCD = LCD_1inch14()
    #color BRG
    LCD.fill(LCD.blue)
 
    LCD.show()
    LCD.text("Poop",120,40,LCD.green if CURRENT == "POOP" else LCD.red)
    LCD.text("Bath",120,60,LCD.green if CURRENT == "BATH" else LCD.red)
    LCD.text("Feed",120,80,LCD.green if CURRENT == "FEED" else LCD.red)
    
    #LCD.hline(10,10,220,LCD.red)
    #LCD.hline(10,125,220,LCD.red)
    #LCD.vline(10,10,115,LCD.red)
    #LCD.vline(230,10,115,LCD.red)

    LCD.show()
    keyA = Pin(15,Pin.IN,Pin.PULL_UP)
    keyB = Pin(17,Pin.IN,Pin.PULL_UP)
    
    key2 = Pin(2 ,Pin.IN,Pin.PULL_UP) #上
    key3 = Pin(3 ,Pin.IN,Pin.PULL_UP)#中
    key4 = Pin(16 ,Pin.IN,Pin.PULL_UP)#左
    key5 = Pin(18 ,Pin.IN,Pin.PULL_UP)#下
    key6 = Pin(20 ,Pin.IN,Pin.PULL_UP)#右
    
    
    while(1):
        if(keyA.value() == 0):
            LCD.fill_rect(208,12,20,20,LCD.red)
            print("A")
        else :
            LCD.fill_rect(208,12,20,20,LCD.white)
            LCD.rect(208,12,20,20,LCD.red)
            
            
        if(keyB.value() == 0):
            LCD.fill_rect(208,103,20,20,LCD.red)
            print("B")
        else :
            LCD.fill_rect(208,103,20,20,LCD.white)
            LCD.rect(208,103,20,20,LCD.red)
    
    
        if(key2.value() == 0):#上
            LCD.fill_rect(37,35,20,20,LCD.red)
            print("UP")
            
            try:
                CURRENT = menu(CURRENT,"UP")
            except Exception:
                pass


        else :
            LCD.fill_rect(37,35,20,20,LCD.white)
            LCD.rect(37,35,20,20,LCD.red)
            
            
        if(key3.value() == 0):#中
            LCD.fill_rect(37,60,20,20,LCD.red)
            print("CTRL")
        else :
            LCD.fill_rect(37,60,20,20,LCD.white)
            LCD.rect(37,60,20,20,LCD.red)
            
        
        if(key4.value() == 0):#左
            LCD.fill_rect(12,60,20,20,LCD.red)
            print("LEFT")
        else :
            LCD.fill_rect(12,60,20,20,LCD.white)
            LCD.rect(12,60,20,20,LCD.red)
            
            
        if(key5.value() == 0):#下
            LCD.fill_rect(37,85,20,20,LCD.red)
            print("DOWN")
            try:
                CURRENT = menu(CURRENT,"DOWN")
            except Exception:
                pass

        else :
            LCD.fill_rect(37,85,20,20,LCD.white)
            LCD.rect(37,85,20,20,LCD.red)
            
            
        if(key6.value() == 0):#右
            LCD.fill_rect(62,60,20,20,LCD.red)
            print("RIGHT")
        else :
            LCD.fill_rect(62,60,20,20,LCD.white)
            LCD.rect(62,60,20,20,LCD.red)

            
        LCD.show()

    time.sleep(1)
    print("END")
    LCD.fill(0xFFFF)


